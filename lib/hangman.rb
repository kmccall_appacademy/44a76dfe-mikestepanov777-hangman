class Hangman
  attr_accessor :guesser, :referee, :board

  def initialize(hash = {:guesser => "Human", :referee => "Computer"})
      @guesser = hash[:guesser]
      @referee = hash[:referee]
    @turns = 10
  end

  def setup
    secret_word_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_word_length)
    @board = [nil] * secret_word_length
  end

  def play
    setup
    until @turns == 0
      take turn
      break if @board.size == @board.compact.size
    end
    if @turns > 0
      "The Guesser won!"
    else
      "The Guesser lost!"
    end
    "The correct word was #{@referee.secret_word}"
  end

  def take_turn
    guess = @guesser.guess
    indexes = @referee.check_guess(guess)
    update_board(guess, indexes)
    @guesser.handle_response(guess, indexes)
  end

  def update_board(guess, indexes)
    indexes.each do |idx|
      @board[idx] = guess
    end
  end

end

class HumanPlayer
  attr_accessor :secret_word_length, :secret_word

  def register_secret_length(length)
    puts "The secret length of the word is #{length}"
  end

  def pick_secret_word
    puts "Please pick write down secret word:"
    @secret_word = gets.chomp
    @secret_word.length
    @already_guessed = []
  end

  def guess(board)
    puts "Already guessed array: #{@already_guessed}"
    puts "Here is the board: #{board}"
    puts "Guess the letter!"
    gets.chomp[0]
  end

  def check_guess(letter)
    letter_indicies = []
    @secret_word.chars.each_with_index do |ch, idx|
      letter_indicies << idx if ch == letter
    end
    letter_indicies
  end

  def handle_response(letter)
    if @secret_word.include?(letter)
      puts "You guessed correctly!"
    else
      puts "You guess incorrectly."
    end
  end
end

class ComputerPlayer
  attr_accessor :dictionary, :board, :candidate_words, :secret_word

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select{|str| str.size == length}
  end

  def check_guess(letter)
    @dictionary[0].chars.map.with_index{|chr, idx| idx if chr == letter}.compact
  end

  def guess(board)
    candidate_arr = @candidate_words.join.chars - board
    candidate_arr.max_by{|chr| candidate_arr.count(chr)}
  end

  def handle_response(letter, indexes)
    (0...@candidate_words[0].size).each do |idx|
      if indexes.include?(idx)
        @candidate_words = @candidate_words.select{|word| word[idx] == letter}
      else
        @candidate_words = @candidate_words.select{|word| word[idx] != letter}
      end
    end
  end

end


# if __FILE__ == $PROGRAM_NAME
#   game = Hangman.new
#   game.play
# end
